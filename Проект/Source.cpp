#include<iostream> 
#include<cmath> 
#include<ctime> 
#include <iostream>
#include <fstream>
using namespace std;
void gauss(double**x, int n,double*g) //������� ������
{
	// ���� ��� ������ ������� ������ 
	for (int k = 0; k < n - 1; ++k)
	{
		// ���� �� ���� ��������, ������� ���� 
		if (x[k][k] == 0) //������ ������� ������, ���� ������������ ������� 0
		{
			for (int i = 0; i < n + 1; ++i)
			{
				swap(x[k][i], x[k+1][i]);
			}
		}
		double el_0 = x[k][k];
		for (int i = k + 1; i < n; ++i)
		{                                               // ������� ������� � ������� ������ 
			double den_c = x[i][k];  // ���� �� ��������� � ������ 
			if (x[i][k] == 0)               //���� ������ ������� � ������ � ������� ��������� =0 �� ��������������� �� ��� �������������
				continue;
			else {
				for (int j = 0; j < n + 1; ++j)
				{

					x[i][j] = x[i][j] * el_0 / den_c - x[k][j];
					if (abs(x[i][j]) < 0.000001)                        //��������� �� ����������� double
						x[i][j] = 0;
				}
			}
		}

	}
	for (int i = n-1; i >= 0; --i)//��������� ����
	{
		double d = x[i][n];;//��������� ���� ����� =
		for (int j = n-1; j >=0; --j) //���� ����� ����� ������� ��������� ��� ����� ����
		{
			
			d = d - (x[i][j] * g[n - j - 1]); //�� ���������� ����� �������� ����������� ��� �����, ��������� �� ����, ���� ��� ����
		}
	    g[n-i-1] = d / x[i][i];//����� ��, ��� �������� �� ����������� ��� ��� � �������� ���
	}
	cout << "GAUSS:" << endl;
	for (int j = n-1; j >=0; --j)//������� ����
	{
		cout << "x" << n - j << "=" << g[j] << " " << endl;
	}
}
double kramer(double** k, int n)  //������� �������
{
	if (n == 1)
	{
		return k[0][0];
	}
	double sum = 0;
	double **minor;
	int decr = 0;
	//���� ��� ������ �������� ��������
	//� 0-� �������
	for (int i = 0; i<n; ++i)
	{
		//��� ������� �������� � �������
		//����� ������������ ��������������� �����
		minor = new double*[n - 1];
		//���� �� ���� ��������
		for (int j = 0; j<n; ++j)
		{
			//���� ������� ������� -- ��� �������,
			//������� �������� ������� �������,
			//�� ������ decr = 1, � ��������� �� ���������
			//(������������� �� ��������� �������)
			if (j == i)
			{
				decr = 1;
				continue;
			}

			//������� ������ ��� ����� � ������
			//���� ����������� ����� �������,
			//�� ������ ��� ���������� � �����
			//����� ��������� �� �������,
			//��� ��� ��� ������ = n-1
			minor[j - decr] = new double[n - 1];
			//���� ��� ���������� ������ � ����� �����������
			//���������� � 1, ��� ��� 0-� ������� �����������
			for (int l = 1; l<n; ++l)
			{
				//��������� � ������ ���������� � ����,
				//������� k-1
				//j-decr -- ��������� ������ �� �������,
				//���� ����������� ����� ������
				minor[j - decr][l - 1] = k[j][l];
			}
		}
		//��� ��������� �������� �������� decr
		decr = 0;
		//��������� ��������� � ����� �� �����������
		//det(minor,n-1) -- ����������� ����������
		//������������ ������
		sum += k[i][0] * pow(-1, i + 0)*kramer(minor, n - 1);
		for (int j = 0; j<n - 1; ++j)
			delete[] minor[j];
		delete[] minor;
	}
	return sum;
	
}
int main(int argc, char *argv[])
{
	if (argc < 3) {
		return 1;
	}

	ifstream in(argv[1]);
	ofstream out(argv[2]);
	in.open("x.txt");
	if (!in.is_open()) //������� ��������� ��
	{
		cout << "error" << endl;
		return 1;
	}
	int n = 0;//����������� �������, ���������� ����������
	int z = 0;
	int t = 0;
	in >> z;
	n = z;
	cout << n << endl;
	double**x = new double*[n];       //��������� ������
	for (int i = 0; i < n; ++i)
	{
		x[i] = new double[n+1];//��������� ������ ��� ����� �� ��������� ����� ����� = - ��������� �����
		for (int j = 0; j < n+1; ++j)
		{
			in >> z;
			x[i][j] = z;
			
			cout << x[i][j]<<" ";
		}
		cout << endl;
	}
	cout << endl;
	double**k = new double*[n];       //��������� ������ ��� ������� ����������� � ������ �������
	for (int i = 0; i < n; ++i)
	{
		k[i] = new double[n];
		for (int j = 0; j < n; ++j)
		{
			k[i][j] = x[i][j]; 
		}
	}
	double*g = new double[n];
	double*kd = new double[n];
	for (int j = 0; j < n ; ++j)//�������� ����
	{
		g[j] = 0; //����� ���������� ������� ������
		kd[j] = 0;//����� ���������� ������� �������
	}
	unsigned int start_time1 = clock(); //�������� ������ �������
	for (int q = 0; q < n+1; ++q)             //������
	{
		if (((q == 0) || (q % 2 == 1)) && (q!=n))//����� ����� ���� ������
			kd[q] = kramer(x, n);
		else
			kd[q] = -kramer(x, n);
		if ((q == n) && (n % 2 != 0))//������ ���� ��������� ������ ���� ������ ���������� ����������
		{
			kd[q] *= -1;
		}
		for (int i = 0; i < n; ++i)  //����������� ��������� ����� � �������� ��� �������� 
		{
			t=x[i][n];
			x[i][n] = x[i][q];
			x[i][q] = t;
			
		}
	}  //������
	t = kd[0]; //������� ������, �� ������� ��� ����� ������
	cout << "KRAMER:" << endl;
	for (int i = 1; i < n+1; ++i)
	{
		kd[i] = kd[i]/t;//���� ��������� ���
		cout << "x" << i << "=" << kd[i] << endl;
	}
	cout << endl;
	unsigned int end_time1 = clock(); // �������� ����� �������
	unsigned int search_time1 = end_time1 - start_time1; //������� �������� ������ �������
	cout << "VREMYS KRAMERA: " << (double) search_time1 / CLOCKS_PER_SEC << endl << endl;
	for (int i = 0; i < n; ++i)                //���������� ��������� ��������
	{
		for (int j = 0; j < n; ++j)
		{
			t = x[i][j];
			x[i][j] = x[i][j + 1];
			x[i][j + 1] = t;
		}
	}
	unsigned int start_time2 = clock(); //�������� ����� ������ 
	gauss(x, n,g);
	unsigned int end_time2 = clock(); // �������� ����� ������
	unsigned int search_time2 = end_time2 - start_time2;//�������� ������ ������
	cout <<endl<< "VREMYA GAUSSA: " << (double)search_time2 / CLOCKS_PER_SEC << endl << endl;
	cout << "TREUGOLNAYA_MATRICA_GAUSSA:" << endl;
	for (int i = 0; i < n; ++i)                //������� ������������ �������
	{
		for (int j = 0; j < n + 1; ++j)
		{

			cout << x[i][j] << " ";
		}
		cout << endl;
	}
	for (int j = 0; j < n; j++)
	{
		delete [] x [j];
		delete [] k [j];
	}
	delete[] x;
	delete[] kd;
	delete[] k;
	delete[] g;
	system("pause");
	return 0;
}